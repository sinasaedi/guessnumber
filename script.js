let guesses = [];
let correctNumber = 0
let incorrectNumber = 0;
let rayconnect
let score = 0;
window.onload = async function () {
    document.getElementById("number-submit").addEventListener("click", playGame);
    document.getElementById("restart-game").addEventListener("click", initGame);


    await connectToServer()

    score = await rayconnect.CloudStorage.getItem("score") || 0
    guesses = await rayconnect.CloudStorage.getItem("history") || []
    const savedCorrectNumber = await rayconnect.CloudStorage.getItem("correct")


    // cloud storage data for fast saving is string convert from string to array and number for really use in my app
    guesses = JSON.parse(guesses)
    score = Number(score)

    if (savedCorrectNumber) {
        correctNumber = Number(savedCorrectNumber)
    } else {
        correctNumber = getRandomNumber();
    }

    setScoreInUI(score);
    displayHistory();




}

function setScoreInUI(scoreValue) {
    const scoreContent = document.querySelector("#score");

    scoreContent.innerHTML = scoreValue;
}

function connectToServer() {
    rayconnect = new Rayconnect({
        scopes: 'game',
        appID: 'guesssina',
        space: 'main',
        type: 'client'
    }, undefined, true);

    return rayconnect.GetGuestAccess()



}

function playGame() {
    let numberGuess = document.getElementById('number-guess').value;
    displayResult(numberGuess);
    saveGuessHistory(numberGuess);
    displayHistory();

}

async function displayResult(numberGuess) {
    console.log(correctNumber)
    if (numberGuess > correctNumber) {
        showNumberAbove();
        incorrectNumber++;
    } else if (numberGuess < correctNumber) {
        showNumberBelow();
        incorrectNumber++;
    } else if (numberGuess == correctNumber) {
        incorrectNumber = 0;

        // get new random number when you won
        correctNumber = getRandomNumber();

        // increment number
        await incrementScore()
        showYouWon();

    }

    if (incorrectNumber >= 4) {
        await decrementScore()
    }
}

function initGame() {
    correctNumber = getRandomNumber();
    document.getElementById("result").innerHTML = "";
    guesses = [];
    displayHistory();


}
/*
function resetResultContent() {
    document.getElementById("result").innerHTML = "";
}
*/
function getRandomNumber() {
    let randomNumber = Math.random();
    let wholeNumber = Math.floor(randomNumber * 100) + 1;
    // console.log(randomNumber)
    // console.log(wholeNumber)

    rayconnect.CloudStorage.setItem("correct", wholeNumber)
    return wholeNumber;
}

async function saveGuessHistory(guess) {
    guesses.push(guess);
    await rayconnect.CloudStorage.setItem("history", guesses);
    console.log(guesses)
}

function displayHistory() {
    let index = guesses.length - 1;
    let list = "<ul class='list-group'>";
    while (index >= 0) {
        list += "<li class='list-group-item'>" + "You guessed " + guesses[index] + "</li>";
        index -= 1;
    }
    list += '</ul>';
    document.getElementById("history").innerHTML = list;
}


function getDialog(dialogType, text) {
    let dialog;
    switch (dialogType) {
        case "warning":
            dialog = "<div class='alert alert-warning' role='alert'>";
            break;
        case "won":
            dialog = "<div class='alert alert-success' role='alert'>";
            break;
    }
    dialog += text;
    dialog += "</div>";
    return dialog;
}


async function showYouWon() {
    const text = "Awesome job , you got it!"


    let dialog = getDialog('won', text);
    console.log(dialog)
    document.getElementById("result").innerHTML = dialog;
}

function incrementScore() {
    score += 2
    setScoreInUI(score);
    return rayconnect.CloudStorage.setItem("score", score)
}

function decrementScore() {
    score--;
    setScoreInUI(score)
    return rayconnect.CloudStorage.setItem("score", score)
}

function showNumberAbove() {
    const text = "your guess is too high ";
    let dialog = getDialog('warning', text)
    document.getElementById("result").innerHTML = dialog;
}

function showNumberBelow() {
    const text = "Your guess is too low!";
    let dialog = getDialog('warning', text);
    document.getElementById("result").innerHTML = dialog;
}
